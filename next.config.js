const webpack = require("webpack");

module.exports = {
  webpack: (config) => {
    config.plugins.push(new webpack.EnvironmentPlugin(process.env));
    return config;
  },
  publicRuntimeConfig: {
    APP_ENVIRONMENT: process.env.NODE_ENV === "production",
  },
};
