import axios from "axios";
import { API_BASE_URL } from "../config";

/**
 * ApiClient
 * based on axios
 */
class ApiClient {
  constructor() {
    this.client = axios.create({
      timeout: 10000,
      baseURL: API_BASE_URL,
    });
  }

  /**
   * sample API request
   */
  async sampleApiRequest() {
    // make API request
    // return this._request("get", "/users", { limit: 5 })
    return { msg: "response from API" };
  }

  /**
   * Perform API request
   *
   * @param   {String}  method  http method, i.e. "get"
   * @param   {String}  url     endpoint url
   * @param   {*}       params  query params
   *
   * @return  {*}               response data
   */
  async _request(method, url, params = {}, data = {}) {
    try {
      const result = await this.client({
        method,
        url,
        params,
        data
      });
      return result.data || {};
    } catch (e) {
      throw new Error(e);
    }
  }
}

export default new ApiClient();
