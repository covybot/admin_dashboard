import { Row, Col } from "react-bootstrap";
import Card from "../common/Card";

const DashboardMetrics = () => {
  return (
    <>
      <h2 className="font-weight-light text-primary mb-4">Key Overview</h2>
      <img src="/img/filter.svg" className="mb-4" />
      <Row className="mb-4">
        <Col xs={12} lg={3}>
          <Card>
            <div className="text-center">
              <img src="/img/stat_1.svg" className="mb-4 img-fluid" />
              <p className="m-0 p-0">Number of Survey Responses</p>
            </div>
          </Card>
        </Col>
        <Col xs={12} lg={3}>
          <Card>
            <div className="text-center">
              <img src="/img/stat_2.svg" className="mb-4 img-fluid" />
              <p className="m-0 p-0">New Respondents with Symptoms</p>
            </div>
          </Card>
        </Col>
        <Col xs={12} lg={6}>
          <Card>
            <div className="text-center">
              <img src="/img/stat_3.svg" className="mb-4 img-fluid" />
              <p className="m-0 p-0">Number of Respondents with Symptoms</p>
            </div>
          </Card>
        </Col>
      </Row>
      <hr className="mt-5 mb-4" />
    </>
  );
};

export default DashboardMetrics;
