import { Row, Col } from "react-bootstrap";
import Card from "../common/Card";

export default () => (
  <Row className="d-flex align-items-center">
    <Col xs={12} lg={8}>
      <img src="/img/barbados.svg" />
    </Col>
    <Col xs={12} lg={4}>
      <Card>
        <p className="lead text-primary font-weight-normal">Filter </p>
        <div className="form-check">
          <input className="form-check-input" type="checkbox" name="filter" value="1" />
          <label className="form-check-label ml-3">Cough</label>
        </div>
        <div className="form-check">
          <input className="form-check-input" type="checkbox" name="filter" value="2" />
          <label className="form-check-label ml-3">Sore Throat</label>
        </div>
        <div className="form-check">
          <input className="form-check-input" type="checkbox" name="filter" value="3" checked="checked" />
          <label className="form-check-label ml-3">Fever</label>
        </div>
        <div className="form-check">
          <input className="form-check-input" type="checkbox" name="filter" value="4" />
          <label className="form-check-label ml-3">Headache</label>
        </div>
      </Card>
    </Col>
  </Row>
);
