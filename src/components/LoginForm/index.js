import { Component } from "react";
import { Col, Row } from "react-bootstrap";
import { countries } from "../../constants";
import { setLocalStorage } from "../../utils";
import Button from "../common/Button";
import Card from "../common/Card";
import Input from "../common/Input";
import Select from "../common/Select";

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.login = this.login.bind(this);
  }

  login() {
    setLocalStorage("loggedIn", true);
    window.location.reload();
  }

  render() {
    return (
      <Row className="d-flex justify-content-center">
        <Col xs={12} lg={4}>
          <div className="text-center">
            <img src="/img/logo_2.svg" height={65} className="mb-5" />
          </div>
          <Card>
            <div className="my-4">
              <div className="form-group">
                <Select name="country">
                  {countries.map((x) => (
                    <option value={x}>{x}</option>
                  ))}
                </Select>
              </div>
              <div className="form-group">
                <Input type="text" placeholder="Email Address" />
              </div>
              <div className="form-group">
                <Input type="password" placeholder="Password" />
              </div>
            </div>
            <Button onClick={this.login}>Login</Button>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default LoginForm;
