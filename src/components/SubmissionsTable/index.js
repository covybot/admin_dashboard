import { Component } from "react";
import { Col, Row, Table } from "react-bootstrap";
import { submission_table_columns } from "../../constants";
import { data } from "../../resources/static";
import Card from "../common/Card";

class SubmissionsTable extends Component {
  constructor(props) {
    super(props);
  }

  renderHeader() {
    return (
      <thead>
        <tr className="text-left">
          {submission_table_columns.map((x) => (
            <th className="text-muted">{x}</th>
          ))}
        </tr>
      </thead>
    );
  }

  renderRows() {
    return data.map((x) => (
      <tr className="text-left">
        <td>{x.client_id}</td>
        <td>{x.submission_id}</td>
        <td>{x.date_submitted}</td>
        <td>{x.first_contacted}</td>
        <td>{x.surveys_completed}</td>
        <td>{x.age}</td>
        <td>{x.sex}</td>
        <td>{x.location}</td>
        <td>{x.has_symptoms}</td>
        <td>&nbsp;</td>
      </tr>
    ));
  }

  renderPagination() {
    return (
      <nav aria-label="Page navigation example">
        <ul className="pagination">
          <li className="page-item">
            <a className="page-link" href="#">
              Previous
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="#">
              1
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="#">
              2
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="#">
              3
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="#">
              Next
            </a>
          </li>
        </ul>
      </nav>
    );
  }

  render() {
    return (
      <Row className="mb-4">
        <Col xs={12}>
          <h2 className="font-weight-light text-primary mb-4">All Survey Submissions</h2>
          <Card noPadding>
            <div className="text-center">
              <Table striped>
                {this.renderHeader()}
                <tbody>{this.renderRows()}</tbody>
              </Table>
            </div>
          </Card>
          <div className="p-3 float-right">{this.renderPagination()}</div>
        </Col>
      </Row>
    );
  }
}

export default SubmissionsTable;
