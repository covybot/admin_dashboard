const Button = (props) => (
  <button className="primary-button" {...props}>
    {props.children}
  </button>
);

export default Button;