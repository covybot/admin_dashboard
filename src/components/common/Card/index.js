export default (props) => (
  <div className="basic-card-wrapper">
    <div className={`${!props.noPadding && "p-4"}`}>{props.children}</div>
  </div>
);
