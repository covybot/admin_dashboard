import Head from "next/head";
import { connect } from "react-redux";
import FullscreenLoader from "../misc/FullscreenLoader";
import Navbar from "../partials/Navbar";
import { APP_NAME } from "../../../config";
import Sidebar from "../partials/Sidebar";

// Main page layout with sidebar aside
const AppLayout = ({ title, description, loading, children }) => {
  return (
    <div id="app">
      <Head>
        <meta property="og:title" content={`${title} - ${APP_NAME}`} />
        <meta property="og:type" content="website" />
        <meta property="og:description" content={description} />
        <title>{`${title} - ${APP_NAME}`}</title>
      </Head>
      <FullscreenLoader visible={loading} />
      <Navbar />
      <Sidebar />
      <div id="wrapper">
        <section id="content-wrapper">
          <div id="content-inner">
            {children}
          </div>
        </section>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  loading: state.common.loading,
});

export default connect(mapStateToProps, null)(AppLayout);
