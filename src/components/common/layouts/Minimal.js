import Head from "next/head";
import { connect } from "react-redux";
import { APP_NAME } from "../../../config";
import FullscreenLoader from "../misc/FullscreenLoader";
import Navbar from "../partials/Navbar";

// Minimal layout for public pages
const MinimalLayout = ({ title, description, loading, children }) => {
  return (
    <div id="minimal-app">
      <Head>
        <meta property="og:title" content={`${title} - ${APP_NAME}`} />
        <meta property="og:type" content="website" />
        <meta property="og:description" content={description} />
        <title>{`${title} - ${APP_NAME}`}</title>
      </Head>
      <FullscreenLoader visible={loading} />
      {/* <Navbar /> */}
      {/* <Sidebar /> */}
      <div id="minimal-wrapper">
        <section id="minimal-content-wrapper">
          <div id="minimal-content-inner" className="p-5">
            {children}
          </div>
        </section>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  loading: state.common.loading,
});

export default connect(mapStateToProps, null)(MinimalLayout);
