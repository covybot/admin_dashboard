import React from "react";
import { Modal } from "reactstrap";
import { Spinner } from "react-bootstrap";

const FullscreenLoader = props => {
  return (
    <>
      <div>
        <Modal
          isOpen={props.visible}
          size="sm"
          className="h-100 d-flex align-items-center"
          contentClassName="bg-transparent border-0"
          fade={false}
        >
          {/* <Spinner style={{ width: '4rem', height: '4rem' }} color="light" className="mx-auto" /> */}
          <Spinner animation="border" variant="light" style={{ width: '4rem', height: '4rem', margin: 'auto' }} />
        </Modal>
      </div>
    </>
  );
};

export default FullscreenLoader;
