import Link from "next/link";
import { APP_ENVIRONMENT } from "../../../config";

// define environment prefixes
const DEVELOPMENT_PREFIX = "";
const PRODUCTION_PREFIX = "";

const ROUTER_PREFIX = APP_ENVIRONMENT === "production" ? PRODUCTION_PREFIX : DEVELOPMENT_PREFIX;

// export router link component
export default (props) => (
  <Link href={props.href && `${ROUTER_PREFIX}${props.href}`} as={props.as && `${ROUTER_PREFIX}${props.as}`}>
    {props.children}
  </Link>
);
