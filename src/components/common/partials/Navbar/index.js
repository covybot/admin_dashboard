import { Component } from "react";
import { Container } from "react-bootstrap";
import { connect } from "react-redux";
import Button from "../../Button";
import { removeLocalStorage, getLocalStorage } from "../../../../utils";

class Navbar extends Component {
  constructor(props) {
    super(props);

    this.logout = this.logout.bind(this);
  }

  renderNavbar() {
    const { loggedIn } = this.props;
    return (
      <div className="w-100 d-flex justify-content-between align-items-center px-2">
        <div>
          <img src="/img/logo.svg" height={50} className="mr-5" />
          <span className="lead">Admin Dashboard</span>
        </div>
        <div className="d-none d-lg-inline-block">
          <span className="pr-3 text-primary">
            Welcome, UNDP Accelerator Lab.
          </span>
          <span>
            <Button onClick={this.logout}>Logout</Button>
          </span>
        </div>
      </div>
    );
  }

  logout() {
    removeLocalStorage('loggedIn');
    console.log(getLocalStorage('loggedIn'))
    window.location.reload();
  }

  render() {
    return (
      <div className="cp-navbar_wrapper shadow-sm fixed-top">
        <Container fluid>{this.renderNavbar()}</Container>
      </div>
    );
  }
}

export default connect()(Navbar);
