import Link from "next/link";
import { useRouter } from "next/router";
import Button from "../../Button";

const Sidebar = () => {
  const router = useRouter();
  const toggleButton = () => alert('Survey data exported as CSV');

  return (
    <div className="sidebar-wrapper">
      <div className="sidebar-inner">
        <Link href="/">
          <p className={`sidebar-link ${router.pathname === "/" && "sidebar-link-active"}`}>Dashboard</p>
        </Link>
        <Link href="/analytics">
          <p className={`sidebar-link ${router.pathname === "/analytics" && "sidebar-link-active"}`}>Analytics</p>
        </Link>
        {/* <Link href="/">
          <p className={`sidebar-link ${router.pathname === "/export" && "sidebar-link-active"}`}>Export Data</p>
        </Link> */}
        <Link href="/">
          <p className={`sidebar-link ${router.pathname === "/settings" && "sidebar-link-active"}`}>Settings</p>
        </Link>
        <Button onClick={toggleButton}>Export Data</Button>
      </div>
    </div>
  );
};

export default Sidebar;
