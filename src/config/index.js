import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();

export const APP_ENVIRONMENT = publicRuntimeConfig.APP_ENVIRONMENT || 'development';
export const APP_NAME = "Covybot";
export const API_BASE_URL = "/";