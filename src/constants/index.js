/**
 * submission table column headers
 */
export const submission_table_columns = [
  "Client ID",
  "Submission ID",
  "Date Submitted",
  "First Contacted",
  "Surveys Completed",
  "Age",
  "Sex",
  "Location",
  "Symptomatic",
];

/**
 * sidebar navigation links
 */
export const sidebar_links = ["Dashboard", "Analytics", "Export Data", "Settings"];

/**
 * caribbean countries
 */
export const countries = [
  'Barbados'
]