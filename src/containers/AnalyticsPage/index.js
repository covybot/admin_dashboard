import { Component } from "react";
import { connect } from "react-redux";
import { Container } from "react-bootstrap";
import dynamic from "next/dynamic";

const InteractiveMap = dynamic(import("../../components/InteractiveMap"), { ssr: false });

class AnalyticsPage extends Component {
  render() {
    return (
      <Container fluid>
        <h2 className="font-weight-light text-primary mb-4">Interactive Map</h2>
        <hr />
        <InteractiveMap />
      </Container>
    );
  }
}

export default connect()(AnalyticsPage);
