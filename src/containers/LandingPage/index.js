import { Component } from "react";
import { connect } from "react-redux";
import { Container } from "react-bootstrap";
import dynamic from "next/dynamic";

const DashboardMetrics = dynamic(import("../../components/DashboardMetrics"), { ssr: false })
const SubmissionsTable = dynamic(import("../../components/SubmissionsTable"), { ssr: false })

class LandingPage extends Component {
  render() {
    return (
      <Container fluid>
        <DashboardMetrics />
        <SubmissionsTable />
        {/* <img src="/img/barbados.svg" /> */}
      </Container>
    );
  }
}

export default connect()(LandingPage);
