import App from "next/app";
import React from "react";
import { Provider } from "react-redux";
import withReduxStore from "../lib/with-redux-store";
import * as config from "../config";
import { PageTransition } from "next-page-transitions";
import { getCurrentUser } from "../redux/actions/auth";

// stylesheets
import "../../public/fonts/fonts.css";
import "../styles/main.scss";

class MyApp extends App {

  render() {
    const { Component, pageProps, reduxStore } = this.props;
    // inject config to global context
    const injectedProps = { ...pageProps, ...config };

    return (
      <Provider store={reduxStore}>
        <PageTransition timeout={300} classNames="page-transition">
          <Component {...injectedProps} />
        </PageTransition>
        <style jsx global>{`
          body {
            background: #f2f6fa;
            padding-top: 100px;
          }
          .page-transition-enter {
            opacity: 0;
          }
          .page-transition-enter-active {
            opacity: 1;
            transition: opacity 500ms;
          }
          .page-transition-exit {
            opacity: 1;
          }
          .page-transition-exit-active {
            opacity: 0;
            transition: opacity 500ms;
          }
        `}</style>
      </Provider>
    );
  }
}

export default withReduxStore(MyApp);
