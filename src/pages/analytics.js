import { Component } from "react";
import { connect } from "react-redux";
import AppLayout from "../components/common/layouts/App";
import AnalyticsPage from "../containers/AnalyticsPage";
import { showLoader, hideLoader } from "../redux/actions/common";
import { getCurrentUser } from "../redux/actions/auth";

class IndexPage extends Component {
  constructor(props) {
    super(props);
    this.props.showLoader();
  }

  componentDidMount() {
    this.props.getCurrentUser();
    setInterval(() => {
      this.props.hideLoader();
    }, 1000);
  }

  render() {
    return (
      <AppLayout title={"Analytics"} description={"Page description"}>
        <AnalyticsPage {...this.props} />
      </AppLayout>
    );
  }
}

export default connect(
  (state) => ({}),
  (dispatch) => ({
    getCurrentUser: () => dispatch(getCurrentUser()),
    showLoader: () => dispatch(showLoader()),
    hideLoader: () => dispatch(hideLoader()),
  })
)(IndexPage);
