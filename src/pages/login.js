import { Component } from "react";
import { connect } from "react-redux";
import MinimalLayout from "../components/common/layouts/Minimal";
import LoginForm from "../components/LoginForm";
import { hideLoader, showLoader } from "../redux/actions/common";
import { getLocalStorage } from "../utils";

class IndexPage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (getLocalStorage("loggedIn")) {
      window.location.href = "/";
    }
  }

  render() {
    return (
      <MinimalLayout title={"Login"} description={"Login to Covybot"}>
        <LoginForm />
      </MinimalLayout>
    );
  }
}

export default connect(
  (state) => ({}),
  (dispatch) => ({
    showLoader: () => dispatch(showLoader()),
    hideLoader: () => dispatch(hideLoader()),
  })
)(IndexPage);
