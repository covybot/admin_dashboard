import { actionTypes } from "../types";
import { getLocalStorage } from "../../utils";

export const login = () => async (dispatch) => {
  dispatch({ type: actionTypes.LOGIN, payload: true });
};

export const logout = () => async (dispatch) => {
  dispatch({ type: actionTypes.LOGOUT, payload: false });
};

export const getCurrentUser = () => async (dispatch) => {
  const loggedIn = getLocalStorage('loggedIn');
  console.log(loggedIn)

  if (!loggedIn) {
    window.location.href = '/login';
    return;
  }

  dispatch({
    type: actionTypes.AUTH_GET_USER,
    payload: loggedIn
  })
};
