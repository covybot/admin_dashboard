import { actionTypes } from "../types";

export const showLoader = () => async (dispatch) =>
  dispatch({ type: actionTypes.SHOW_LOADER });

export const hideLoader = () => async (dispatch) =>
  dispatch({ type: actionTypes.HIDE_LOADER });

export const checkStatus = () => async (dispatch) =>
  dispatch({ type: actionTypes.CHECK_STATUS, payload: true });
