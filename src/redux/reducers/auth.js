import { actionTypes } from "../types";

// initial state
const initialState = {
  loggedIn: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGIN:
      return { ...state, loggedIn: true };
    case actionTypes.LOGOUT:
      return { ...state, loggedIn: false };
    case actionTypes.AUTH_GET_USER:
      return { ...state, loggedIn: action.payload };
    default:
      return state;
  }
};
