import { actionTypes } from "../types";

// initial state for contact
const initialState = {
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SHOW_LOADER:
      return { ...state, loading: true };
    case actionTypes.HIDE_LOADER:
      return { ...state, loading: false };
    case actionTypes.CHECK_STATUS:
      return { ...state, status: action.payload };
    default:
      return state;
  }
};
