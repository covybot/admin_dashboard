const package = require("../package.json");
const express = require("express");
const next = require("next");
const routes = require("./server/routes");

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const port = process.env.PORT || 3000;

const startServer = async () => {
  try {
    await app.prepare();
    const server = express();

    // server config
    server.use(express.json());
    server.use(express.urlencoded({ extended: true }));
    server.set("trust proxy", true);

    // use routes
    server.use(routes);

    // next.js route handler
    server.get("*", (req, res) => {
      return handle(req, res);
    });

    // start server listening on port
    server.listen(port, (err) => {
      if (err) throw err;
      console.log(`> [${package.name}] Listening on :${port}`);
    });
  } catch (e) {
    console.error(`[${package.name}] ${e.message}`, e.stack);
    process.exit(1);
  }
};

startServer();
