class ResourceController {
  /**
   * ping server
   */
  async pingServer(_, res) {
    res.send({ msg: "pong", timestamp: `${new Date().toISOString()}` });
  }
}

module.exports = new ResourceController();
