const router = require("express").Router();
const ResourceController = require('../controllers/resource');

/**
 * ping server
 */
router.get('/server/ping', ResourceController.pingServer);

module.exports = router;