/**
 * get key value from localstorage
 * @param {*} key
 */
export const getLocalStorage = (key) => {
  if (typeof window !== "undefined") {
    return window.localStorage.getItem(key);
  }
};

/**
 * set value for key in localstorage
 * @param {*} key
 * @param {*} value
 */
export const setLocalStorage = (key, value) => {
  if (typeof window !== "undefined") {
    return window.localStorage.setItem(key, value);
  }
};

/**
 * remove key under localstorage
 * @param {*} key
 */
export const removeLocalStorage = (key) => {
  if (typeof window !== "undefined") {
    return window.localStorage.removeItem(key);
  }
};
